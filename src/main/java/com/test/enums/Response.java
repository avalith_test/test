package com.test.enums;

import org.springframework.http.HttpStatus;

public enum Response {
	success("success", HttpStatus.OK, null), error001("invalid data", HttpStatus.BAD_REQUEST, null),
	error002("user email duplicate", HttpStatus.BAD_REQUEST, null),
	error003("The area does not exist", HttpStatus.BAD_REQUEST, null),
	error004("You can't vote 2 times in the same area on this month", HttpStatus.BAD_REQUEST, null),
	error005("You can't vote for your self", HttpStatus.BAD_REQUEST, null),
	error006("The employee does not exist", HttpStatus.BAD_REQUEST, null),
	error007("The employe voted does not exist in the vote area", HttpStatus.BAD_REQUEST, null),
	error008("You should add votdate to filter format yyyy-MM", HttpStatus.BAD_REQUEST, null);

	private String mensaje;
	private HttpStatus status;
	private Object customObject;

	Response(String mensaje, HttpStatus status, Object customObject) {
		this.mensaje = mensaje;
		this.status = status;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Object getCustomObject() {
		return customObject;
	}

	public void setCustomObject(Object customObject) {
		this.customObject = customObject;
	}
}
