package com.test.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Repository
public class Vote {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "votid", length = 50)
	private Integer votid;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "areid", referencedColumnName = "areid", nullable = false)
	private Area area;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Transient
	private Integer areid;

	/**
	 * voting employee
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "empid", referencedColumnName = "empid", nullable = false)
	private Employee employee;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Transient
	private Integer empid;

	/**
	 * Voted employed
	 */
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "empidvoted", referencedColumnName = "empid", nullable = false)
	private Employee Employeevoted;

	@Transient
	private Integer empidvoted;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "votdate", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date votdate;

	@Column(name = "votcomment", nullable = true)
	private String votcomment;

	public Integer getVotid() {
		return votid;
	}

	public void setVotid(Integer votid) {
		this.votid = votid;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Integer getAreid() {
		return areid;
	}

	public void setAreid(Integer areid) {
		this.areid = areid;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getEmpid() {
		return empid;
	}

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	public Date getVotdate() {
		return votdate;
	}

	public void setVotdate(Date votdate) {
		this.votdate = votdate;
	}

	public String getVotcomment() {
		return votcomment;
	}

	public void setVotcomment(String votcomment) {
		this.votcomment = votcomment;
	}

	public Employee getEmployeevoted() {
		return Employeevoted;
	}

	public void setEmployeevoted(Employee employeevoted) {
		Employeevoted = employeevoted;
	}

	public Integer getEmpidvoted() {
		return empidvoted;
	}

	public void setEmpidvoted(Integer empidvoted) {
		this.empidvoted = empidvoted;
	}

}
