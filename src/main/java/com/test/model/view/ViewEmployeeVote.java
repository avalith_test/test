package com.test.model.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Immutable
@Subselect("SELECT * from vote")
public class ViewEmployeeVote {

	@Id
	Integer empid;

	@Column
	String empname;

	@Column
	String emplastname;

	@Column
	String arename;

	@JsonFormat(pattern = "yyyy-MM")
	@Column
	Date votdate;

	@Column
	Integer votes;

	public Integer getEmpid() {
		return empid;
	}

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public String getEmplastname() {
		return emplastname;
	}

	public void setEmplastname(String emplastname) {
		this.emplastname = emplastname;
	}

	public String getArename() {
		return arename;
	}

	public void setArename(String arename) {
		this.arename = arename;
	}

	public Date getVotdate() {
		return votdate;
	}

	public void setVotdate(Date votdate) {
		this.votdate = votdate;
	}

	public Integer getVotes() {
		return votes;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}

}
