package com.test.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Repository
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "empid", length = 50)
	private Integer empid;

	@Column(name = "empidentification", length = 20, nullable = false)
	private String empidentification;

	@Column(name = "empidentificationtype", length = 20, nullable = false)
	private String empidentificationtype;

	@Column(name = "empname", length = 150, nullable = false)
	private String empname;

	@Column(name = "emplastname", length = 150, nullable = false)
	private String emplastname;

	@Column(name = "empphone", length = 20, nullable = true)
	private String empphone;

	@Column(name = "empaddress", length = 200, nullable = true)
	private String empaddress;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "empbirthday", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date empbirthday;

	@ManyToOne
	@JoinColumn(name = "areid", referencedColumnName = "areid", nullable = false)
	private Area area;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Transient
	private Integer areid;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "useid", referencedColumnName = "useid")
	private User user;

	public Employee() {

	}

	public Employee(Integer empid) {
		this.empid = empid;
	}

	public Integer getEmpid() {
		return empid;
	}

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	public String getEmpidentification() {
		return empidentification;
	}

	public void setEmpidentification(String empidentification) {
		this.empidentification = empidentification;
	}

	public String getEmpidentificationtype() {
		return empidentificationtype;
	}

	public void setEmpidentificationtype(String empidentificationtype) {
		this.empidentificationtype = empidentificationtype;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public String getEmplastname() {
		return emplastname;
	}

	public void setEmplastname(String emplastname) {
		this.emplastname = emplastname;
	}

	public String getEmpphone() {
		return empphone;
	}

	public void setEmpphone(String empphone) {
		this.empphone = empphone;
	}

	public String getEmpaddress() {
		return empaddress;
	}

	public void setEmpaddress(String empaddress) {
		this.empaddress = empaddress;
	}

	public Date getEmpbirthday() {
		return empbirthday;
	}

	public void setEmpbirthday(Date empbirthday) {
		this.empbirthday = empbirthday;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getAreid() {
		return areid;
	}

	public void setAreid(Integer areid) {
		this.areid = areid;
	}

}
