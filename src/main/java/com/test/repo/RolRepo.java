package com.test.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.model.Rol;

@Repository
public interface RolRepo extends JpaRepository<Rol, Integer> {

	@Query(value = "SELECT r.* FROM rol as r INNER JOIN user_rol as ur on r.rolid=ur.rolid WHERE ur.useid = ?1", nativeQuery = true)
	List<Rol> findByUserId(Integer usuid);

	Rol findByRolname(String rolname);

}
