package com.test.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.model.Vote;

@Repository
public interface VoteRepo extends JpaRepository<Vote, Integer> {

	/**
	 * Find votes on determinate area and employee in the current month
	 * 
	 * @param areid      id from area
	 * @param employeeid id from employee
	 * @return
	 */
	@Query(value = "SELECT v.* FROM vote as v WHERE v.areid = ?1 AND v.empid = ?2 and year(v.votdate) = year(?3) and  month(v.votdate) = month(?3)", nativeQuery = true)
	List<Vote> findByAreaDate(Integer areid, Integer employeeid, Date fecha);
}
