package com.test.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.model.Area;

@Repository
public interface AreaRepo extends JpaRepository<Area, Integer> {

	Area findByArename(String arename);
}
