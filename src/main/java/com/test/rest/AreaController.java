package com.test.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.test.enums.Response;
import com.test.model.Area;
import com.test.service.AreaService;

@RestController
@RequestMapping("/area")
public class AreaController {

	@Autowired
	private AreaService service;

	/**
	 * Get a list of Areas
	 * 
	 * @return
	 */
	@GetMapping
	public List<Area> list() {
		return service.repo.findAll();
	}

	@PostMapping
	public ResponseEntity<String> add(@RequestBody Area area) {
		Response status = service.create(area);
		if (status.equals(Response.success)) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(status.getStatus()).body(status.getMensaje());
	}

	@PutMapping
	public ResponseEntity<String> update(@RequestBody Area area) {
		Response status = service.update(area);
		if (status.equals(Response.success)) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(status.getStatus()).body(status.getMensaje());

	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable("id") Integer id) {
		service.repo.deleteById(id);
	}

}
