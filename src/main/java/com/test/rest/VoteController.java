package com.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.test.enums.Response;
import com.test.model.Vote;
import com.test.service.VoteService;

@RestController
@RequestMapping("/vote")
public class VoteController {

	@Autowired
	private VoteService service;

	@PostMapping
	public ResponseEntity<String> add(@RequestBody Vote area) {
		Response status = service.create(area);
		if (status.equals(Response.success)) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(status.getStatus()).body(status.getMensaje());
	}

}
