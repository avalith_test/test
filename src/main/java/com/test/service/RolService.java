package com.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.enums.Response;
import com.test.model.Rol;
import com.test.repo.RolRepo;

/**
 * Any logical business over entity rol is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class RolService {
	@Autowired
	public RolRepo repo;

	public Response create(Rol rol) {
		if (rol != null && rol.getRolname() != null) {
			Rol current = repo.findByRolname(rol.getRolname());
			if (current == null) {
				current = repo.save(rol);
			}
			if (current != null) {
				Response status = Response.success;
				status.setCustomObject(current);
				return status;
			}
		}
		return Response.error001;
	}

	public Response update(Rol objeto) {
		if (objeto != null && objeto.getRolname() != null && objeto.getRolid() != null) {
			if (!this.repo.findById(objeto.getRolid()).isPresent()) {
				return Response.error001;
			} else {
				this.repo.save(objeto);
				return Response.success;
			}

		}
		return Response.error001;
	}
}
