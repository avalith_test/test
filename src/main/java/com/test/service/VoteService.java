package com.test.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.enums.Response;
import com.test.model.Area;
import com.test.model.Employee;
import com.test.model.Vote;
import com.test.repo.VoteRepo;

/**
 * Any logical business over entity Area is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class VoteService {
	@Autowired
	public VoteRepo repo;

	@Autowired
	public EmployeeService serviceEmployee;
	@Autowired
	public AreaService serviceArea;

	public Response create(Vote objeto) {
		Response status = validate(objeto);
		if (status.equals(Response.success)) {
			if (objeto.getVotcomment() == null)
				objeto.setVotcomment("");
			if (this.repo.save(objeto) != null) {
				return Response.success;
			} else {
				return Response.error001;
			}
		}

		return status;
	}

	private Response validate(Vote objeto) {
		if (objeto == null)
			return Response.error001;

		/* Validate if area exists */
		objeto.setArea(new Area(objeto.getAreid()));
		Response statusArea = this.serviceArea.validateExist(objeto.getArea());
		if (!statusArea.equals(Response.success)) {
			return statusArea;
		} else {
			objeto.setArea((Area) statusArea.getCustomObject());
		}

		/* Validate if employee exists */
		objeto.setEmployee(new Employee(objeto.getEmpid()));
		Response statusEmployee = this.serviceEmployee.validateExist(objeto.getEmployee());
		if (!statusEmployee.equals(Response.success)) {
			return statusEmployee;
		} else {
			objeto.setEmployee((Employee) statusEmployee.getCustomObject());
		}

		/* Validate if employee voted exists */
		objeto.setEmployeevoted(new Employee(objeto.getEmpidvoted()));
		Response statusEmployeeVoted = this.serviceEmployee.validateExistOnArea(objeto.getEmployeevoted(),
				objeto.getAreid());
		if (!statusEmployeeVoted.equals(Response.success)) {
			return statusEmployeeVoted;
		} else {
			objeto.setEmployeevoted((Employee) statusEmployeeVoted.getCustomObject());
		}

		/* Validate vote for my self */
		if (objeto.getEmpid().equals(objeto.getEmpidvoted()))
			return Response.error005;
		/* Validate one vote for area on this month */
		objeto.setVotdate(new Date());
		if (!this.repo.findByAreaDate(objeto.getAreid(), objeto.getEmpid(), objeto.getVotdate()).isEmpty())
			return Response.error004;

		return Response.success;
	}

}
