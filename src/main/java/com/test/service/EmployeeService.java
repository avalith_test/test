package com.test.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.enums.Response;
import com.test.enums.Roles;
import com.test.model.Area;
import com.test.model.Employee;
import com.test.model.Rol;
import com.test.repo.EmployeeRepo;

/**
 * Any logical business over entity Employee is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class EmployeeService {
	@Autowired
	public EmployeeRepo repo;

	@Autowired
	public UserService serviceUser;

	@Autowired
	public RolService serviceRol;

	@Autowired
	public AreaService serviceArea;

	public Response create(Employee objeto) {
		Response validate = validarDatos(objeto, true);
		if (validate.equals(Response.success)) {
			objeto.setUser(serviceUser.repo.save(objeto.getUser()));
			objeto = this.repo.save(objeto);
			if (objeto != null) {
				Rol rolemployee = serviceRol.repo.findByRolname(Roles.Employee.name());
				objeto.getUser().setRolCollection(new ArrayList<Rol>());
				objeto.getUser().getRolCollection().add(rolemployee);
				serviceUser.repo.save(objeto.getUser());
			}

			if (objeto != null) {
				return Response.success;
			} else {
				return Response.error001;
			}
		} else {
			return validate;
		}
	}

	public Response update(Employee objeto) {
		Response validate = validarDatos(objeto, false);
		if (validate.equals(Response.success)) {
			objeto = this.repo.save(objeto);
			if (objeto != null) {
				return Response.success;
			} else {
				return Response.error001;
			}
		} else {
			return validate;
		}
	}

	/**
	 * Validate object data
	 * 
	 * @param objeto    object to validate
	 * @param newObject if you are creating a new employee send true else false
	 * @return
	 */
	private Response validarDatos(Employee objeto, Boolean newObject) {
		if (objeto == null)
			return Response.error001;
		if (!newObject && objeto.getEmpid() == null)
			return Response.error001;

		/* Validate Area */
		objeto.setArea(new Area(objeto.getAreid()));
		Response statusArea = this.serviceArea.validateExist(objeto.getArea());
		if (!statusArea.equals(Response.success)) {
			return statusArea;
		} else {
			objeto.setArea((Area) statusArea.getCustomObject());
		}

		if (objeto.getEmpname() == null || objeto.getEmplastname() == null || objeto.getEmpidentification() == null
				|| objeto.getEmpidentificationtype() == null || objeto.getEmpname().isEmpty()
				|| objeto.getEmplastname().isEmpty() || objeto.getEmpidentification().isEmpty()
				|| objeto.getEmpidentificationtype().isEmpty())
			return Response.error001;

		/* Validate User */
		Response status = this.serviceUser.validate(objeto.getUser(), newObject);
		if (!status.equals(Response.success)) {
			return status;
		}

		return Response.success;
	}

	/**
	 * Validate if employee exists
	 * 
	 * @param objeto   blank object to fill with result
	 * @param objeto   employ id
	 * @param validate employee to voted (true) or employ (false)
	 * @return
	 */
	public Response validateExist(Employee objeto) {
		if (objeto == null || objeto.getEmpid() == null)
			return Response.error001;
		Optional<Employee> validateEmployee = this.repo.findById(objeto.getEmpid());
		if (!validateEmployee.isPresent()) {
			return Response.error006;
		} else {
			Response status = Response.success;
			status.setCustomObject(validateEmployee.get());
			return status;
		}
	}

	/**
	 * Validate if employee exists on area
	 * 
	 * @param objeto   blank object to fill with result
	 * @param objeto   employ id
	 * @param validate employee to voted (true) or employ (false)
	 * @return
	 */
	public Response validateExistOnArea(Employee objeto, Integer areid) {
		if (objeto == null || objeto.getEmpid() == null)
			return Response.error001;
		Employee validateEmployee = this.repo.findByArea(areid, objeto.getEmpid());
		if (validateEmployee == null) {
			return Response.error007;
		} else {
			Response status = Response.success;
			status.setCustomObject(validateEmployee);
			return status;
		}
	}

}
