package com.test.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.enums.Response;
import com.test.model.Area;
import com.test.repo.AreaRepo;

/**
 * Any logical business over entity Area is put in this class
 * 
 * @author jeiss
 *
 */
@Service
public class AreaService {
	@Autowired
	public AreaRepo repo;

	public Response create(Area objeto) {
		if (objeto != null && objeto.getArename() != null) {
			Area objetoValid = this.repo.findByArename(objeto.getArename());
			if (objetoValid == null) {
				objetoValid = this.repo.save(objeto);
			}
			if (objetoValid != null) {
				return Response.success;
			}

		}
		return Response.error001;
	}

	public Response update(Area objeto) {
		if (objeto != null && objeto.getArename() != null && objeto.getAreid() != null) {
			if (!this.repo.findById(objeto.getAreid()).isPresent()) {
				return Response.error001;
			} else {
				this.repo.save(objeto);
				return Response.success;
			}

		}
		return Response.error001;
	}

	/**
	 * Validate if area exist
	 * 
	 * @param objeto blank object to fill with result
	 * @param areid  id from area
	 * @return
	 */
	public Response validateExist(Area objeto) {
		if (objeto == null || objeto.getAreid() == null)
			return Response.error001;
		Optional<Area> validateArea = this.repo.findById(objeto.getAreid());
		if (!validateArea.isPresent()) {
			return Response.error003;
		} else {
			Response status = Response.success;
			status.setCustomObject(validateArea.get());
			return status;
		}
	}
}
